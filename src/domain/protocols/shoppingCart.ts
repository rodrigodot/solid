type CartItem = {
  name: string
  price: number
}

type OrderStatus = boolean

interface ShoppingCartProtocol {
  items: Readonly<CartItem[]>
  addItem(item: CartItem): void
  removeItem(index: number): void
  getTotal(): number
  getTotalWithDiscount(): number
  getDiscount(): number
  isEmpty(): boolean
  clear(): void
}

export { CartItem, OrderStatus, ShoppingCartProtocol }
