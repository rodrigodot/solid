import { CartItem } from '../../domain/protocols/shoppingCart'

interface OrderProtocol {
  checkout(): void
  showReceipt(): Promise<void>
  addItem(item: CartItem): void
  removeItem(index: number): void
}

export { OrderProtocol }
