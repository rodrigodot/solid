type PersistencyProtocol = {
  saveOrder(): Promise<boolean>
}

export { PersistencyProtocol }
