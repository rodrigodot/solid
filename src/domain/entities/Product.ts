import { CartItem } from '../protocols/shoppingCart'

export class Product implements CartItem {
  constructor(public name: string, public price: number) {
    //
  }
}
