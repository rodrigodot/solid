import { CartItem } from '../protocols/shoppingCart'
import { WithDiscount } from './Discount'

import { MessengerProtocol } from '../protocols/messenger'
import { ShoppingCartProtocol } from '../protocols/shoppingCart'

export class ShoppingCart implements ShoppingCartProtocol {
  constructor(
    private readonly messenger: MessengerProtocol,
    private readonly withDiscount: WithDiscount,
  ) {
    //
  }
  private readonly _items: CartItem[] = []

  addItem(item: CartItem): void {
    this._items.push(item)
  }

  removeItem(index: number): void {
    if (!this._items[index]) return this.messenger.sendMessage('Produto não encontrado no carrinho')
    this._items.splice(index, 1)
  }

  getTotal(): number {
    const total = this._items.reduce((total, item) => total + item?.price, 0)
    return +total.toFixed(2)
  }

  getTotalWithDiscount(): number {
    return this.withDiscount.calculateDiscount(this.getTotal())
  }

  getDiscount(): number {
    return this.withDiscount.calculateDiscountValue(this.getTotal())
  }

  isEmpty(): boolean {
    return this._items.length === 0
  }

  clear(): void {
    this._items.length = 0
  }

  get items(): Readonly<CartItem[]> {
    return this._items
  }
}
