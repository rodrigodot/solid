export abstract class Discount {
  constructor(protected discount?: number) {
    if (!this.discount) this.discount = 0
    if (this.discount && this.discount > 100) this.discount = 100
  }
  abstract calculateDiscount(value: number): number
  abstract calculateDiscountValue(value: number): number
}

export class WithDiscount extends Discount {
  calculateDiscount(value: number): number {
    return value - value * this.discount
  }
  calculateDiscountValue(value: number): number {
    return value * this.discount
  }
}
