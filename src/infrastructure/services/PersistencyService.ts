import { PersistencyProtocol } from '../../domain/protocols/persistency'

export class PersistencyService implements PersistencyProtocol {
  async saveOrder(): Promise<boolean> {
    return new Promise((resolve) => {
      setTimeout(() => {
        return resolve(true)
      }, 1000)
    })
  }
}
