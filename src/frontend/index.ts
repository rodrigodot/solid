import {
  ShoppingCart,
  Order,
  Product,
  WithDiscount,
  Customer,
  CustomerWithCPF,
  CustomerWithCNPJ,
} from '../domain/entities'

import { Messenger, PersistencyService } from '../infrastructure/services'

const messageService = (message: string) => console.log(message)

const customerSimple = new Customer('rodrigo simple', 'rua 2')
const customerWithCPF = new CustomerWithCPF('12354', 'rua 3', 'rodrigo', 'costa')
const customerWithCNPJ = new CustomerWithCNPJ('245645', 'rua 5', 'rodrigo', '2556666666')

const persistencyService = new PersistencyService()
const withDiscount = new WithDiscount()
const messenger = new Messenger(messageService)
const cart = new ShoppingCart(messenger, withDiscount)
const order = new Order(customerWithCPF, messenger, persistencyService, cart)

order.addItem(new Product('produto1', 10))
order.addItem(new Product('produto2', 20))
order.addItem(new Product('produto3', 30))

order.checkout()

setTimeout(() => {
  // should throw error messages
  order.addItem(new Product('produto4', 40))
  order.removeItem(2)
}, 2000)
