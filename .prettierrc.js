module.exports = {
    semi: false,
    trailingComma: 'all',
    endOfLine: 'auto',
    singleQuote: true,
    printWidth: 100,
    spaces: 2,
}