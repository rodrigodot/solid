import {
  CostumerProtocol,
  CustomerSimple,
  CustomerEnterprise,
  OrderCustomer,
} from '../protocols/customer'

export class Customer implements CostumerProtocol {
  constructor(readonly name: string, readonly endereco: string) {}
}

export class CustomerWithCPF implements CostumerProtocol, CustomerSimple, OrderCustomer {
  constructor(
    readonly cpf: string,
    readonly endereco: string,
    readonly name: string,
    readonly lastName: string,
  ) {}

  getName(): string {
    return `${this.name} ${this.lastName}`
  }
  getIDN(): string {
    return this.cpf
  }
}

export class CustomerWithCNPJ implements CostumerProtocol, CustomerEnterprise, OrderCustomer {
  constructor(
    readonly cnpj: string,
    readonly endereco: string,
    readonly name: string,
    readonly registry: string,
  ) {}

  getName(): string {
    return this.name
  }
  getIDN(): string {
    return this.cnpj
  }
}
