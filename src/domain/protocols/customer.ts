type OrderCustomer = {
  getName(): string
  getIDN(): string
}

type CostumerProtocol = {
  name: string
  endereco: string
}

type CustomerSimple = {
  cpf: string
  lastName: string
}

type CustomerEnterprise = {
  cnpj: string
  registry: string
}

export { CostumerProtocol, CustomerSimple, CustomerEnterprise, OrderCustomer }
