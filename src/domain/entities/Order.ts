import { OrderStatus } from '../protocols/shoppingCart'

import {
  CostumerProtocol,
  CustomerSimple,
  CustomerEnterprise,
  OrderCustomer,
} from '../protocols/customer'

import { MessengerProtocol } from '../protocols/messenger'
import { PersistencyProtocol } from '../protocols/persistency'
import { ShoppingCartProtocol, CartItem } from '../protocols/shoppingCart'
import { OrderProtocol } from '../protocols/order'

export class Order implements OrderProtocol {
  constructor(
    private readonly customer: CostumerProtocol &
      OrderCustomer &
      (CustomerSimple | CustomerEnterprise),
    private readonly messenger: MessengerProtocol,
    private readonly persistencyService: PersistencyProtocol,
    readonly shoppingCart: ShoppingCartProtocol,
  ) {
    this._orderIDN = this.customer.getIDN() + '_' + new Date().getTime()
    this._orderStatus = true
  }

  private _orderIDN: string
  private _orderStatus: OrderStatus

  async showReceipt(): Promise<void> {
    return new Promise((resolve) => {
      const subtotal = {
        name: 'subtotal',
        price: this.shoppingCart.getTotal(),
      }
      const discount = {
        name: 'desconto',
        price: this.shoppingCart.getDiscount(),
      }
      const total = {
        name: 'total',
        price: this.shoppingCart.getTotalWithDiscount(),
      }

      this.shoppingCart.addItem(subtotal)
      this.shoppingCart.addItem(discount)
      this.shoppingCart.addItem(total)
      console.table(this.shoppingCart.items)
      return resolve()
    })
  }

  async checkout(): Promise<void> {
    if (this.shoppingCart.isEmpty()) {
      this.messenger.sendMessage('O carrinho está vazio, portanto não é necessário fechar a venda')
    }

    this._orderStatus = false
    await this.showReceipt()
    const persisted = await this.persistencyService.saveOrder()

    if (!persisted) {
      this._orderStatus = true
      return this.messenger.sendMessage('O Pedido não pode salvo')
    }

    this.messenger.sendMessage(`Caro ${this.customer.getName()}, Pedido salvo`)
    this.messenger.sendMessage(
      `Seu pedido: ${this._orderIDN}, foi recebido e está sendo processado`,
    )
    this.shoppingCart.clear()
  }

  get orderStatus(): OrderStatus {
    return this._orderStatus
  }

  set orderStatus(status: boolean) {
    this._orderStatus = status
  }

  addItem(item: CartItem): void {
    if (!this._orderStatus) {
      const errorMessage = `Não há compras em andamento \n não foi possível adicionar o item ${item?.name} no carrinho`

      // throw new Error(errorMessage)
      return this.messenger.sendMessage(errorMessage)
    }

    this.shoppingCart.addItem(item)
  }

  removeItem(index: number): void {
    if (!this._orderStatus) {
      const errorMessage =
        'Não há compras em andamento \n não foi possível remover o item do carrinho'

      // throw new Error(errorMessage)
      return this.messenger.sendMessage(errorMessage)
    }

    this.shoppingCart.removeItem(index)
  }
}
