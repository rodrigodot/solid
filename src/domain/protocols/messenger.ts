type Message = string
type MessengerProtocol = {
  sendMessage(message: string): void
}

type MessageService = (message: string) => void

export { Message, MessengerProtocol, MessageService }
