import { CartItem } from '../../domain/protocols/shoppingCart'

export class ShoppingCart {
  private readonly _items: CartItem[] = []
  private _orderStatus = true

  addItem(item: CartItem): void {
    if (!this._orderStatus) {
      return this.sendMessage(
        `Não há compras em andamento \n não foi possível adicionar o item ${item.name} no carrinho`,
      )
    }
    this._items.push(item)
  }

  removeItem(index: number): void {
    if (!this._items[index]) return this.sendMessage('Produto não encontrado no carrinho')
    if (!this._orderStatus) {
      return this.sendMessage(
        `Não há compras em andamento \n não foi possível remover o item ${this._items[index]} no carrinho`,
      )
    }
    this._items.splice(index, 1)
  }

  getTotal(): number {
    const total = this._items.reduce((total, item) => total + item.price, 0)
    return +total.toFixed(2)
  }

  isEmpty(): boolean {
    return this._items.length === 0
  }

  clear(): void {
    this._items.length = 0
  }

  sendMessage(message: string): void {
    console.log(message)
  }

  async saveOrder(): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(() => {
        this.sendMessage('Pedido salvo')
        return resolve()
      }, 1000)
    })
  }

  async showReceipt(): Promise<void> {
    return new Promise((resolve) => {
      const total = {
        name: 'total',
        price: this.getTotal(),
      }
      this._items.push(total)
      console.table(this._items)
      return resolve()
    })
  }

  async checkout(): Promise<void> {
    if (this.isEmpty()) {
      this.sendMessage('O carrinho está vazio, portanto não é necessário fechar a venda')
    }

    this._orderStatus = false
    await this.showReceipt()
    await this.saveOrder()
    this.sendMessage('Seu pedido foi recebido e está sendo processado')
    this.clear()
  }

  get items(): Readonly<CartItem[]> {
    return this._items
  }
}
