import { Message, MessageService, MessengerProtocol } from '../../domain/protocols/messenger'

export class Messenger implements MessengerProtocol {
  constructor(private messageService: MessageService) {
    //
  }

  sendMessage(message: Message): void {
    this.messageService(message)
  }
}
